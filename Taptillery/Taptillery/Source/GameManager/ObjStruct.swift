//
//  ObjStruct.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-04-12.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

class ObjStruct {
    var vertices: [Vertex] = []
    var indices: [GLuint] = []
    var index: GLuint = 0
    var texture: GLuint = 0
    
    init() {}
}
