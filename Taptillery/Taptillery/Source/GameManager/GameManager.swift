//
//  GameManager.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-03-07.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

public class GameManager {
    private let player1 : Player! = Player(id : 1)
    private let player2 : Player! = Player(id : 2)
    private var curPlayer : Player!
    private var attackManager : AttackManager = AttackManager()
    private var moveManager : MovementManager = MovementManager()
    private var textureManager : TextureManager!
    private var unitHealth : Int!           // For the current selected unit
    private var unitAttackRange : Int!      // For the current selected unit
    private var unitMoveRange : Int!        // For the current selected unit
    private var sizeOfArmies :Int! = 5
    private var ids : Int = 0
    private var unitManager : [Int : Unit] = [:]
    private var grid : GridController!
    private var uiManager: UIManager!
    private var player1StartPos : [[Int]]! = [[2,2],[3,2],[4,2],[2,1], [3,1], [4,1]]
    private var player2StartPos : [[Int]]! = [[2,10],[3,10],[4,10],[2,11], [3,11], [4,11]]
    var shader : BaseEffect!
    var movementCells : [Cell]!
    var attackCells : [Cell]!
    var destination : GLKVector3! = GLKVector3(v: (0.0, 0.0, 0.0))
    var curCell : Cell!
    var destCell : Cell!
    var finalScore : Int! = 0
    var gameOver : Bool! = false
    
    
    // Initialize dictionary set of [Int: Unit] & instantiate units
    // Note: each child of Unit() takes its unitID in its constructor
    private var unitDB1 : [Int: Unit]  =  [0 : Infantry(id : 0), 1 : Infantry(id : 1), 2 : Infantry(id : 2), 3 : Infantry(id : 3),
                                        4 : Infantry(id : 4), 5 : Infantry(id : 5), 6 : Infantry(id : 6), 7 : Artillery(id : 7),
                                        8 : Artillery(id : 8), 9 : Artillery(id : 9), 10 : Artillery(id : 10), 11 : Artillery(id : 11),
                                        12 : Tank(id : 12), 13 : Tank(id : 13), 14 : Tank(id : 14)]
    
    private var unitDB2 : [Int: Unit]  =  [0 : Infantry(id : 0), 1 : Infantry(id : 1), 2 : Infantry(id : 2), 3 : Infantry(id : 3),
                                           4 : Infantry(id : 4), 5 : Infantry(id : 5), 6 : Infantry(id : 6), 7 : Artillery(id : 7),
                                           8 : Artillery(id : 8), 9 : Artillery(id : 9), 10 : Artillery(id : 10), 11 : Artillery(id : 11),
                                           12 : Tank(id : 12), 13 : Tank(id : 13), 14 : Tank(id : 14)]
    
    
    
    init(grid: GridController, be: BaseEffect, uiManager: UIManager, textureManager: TextureManager) {
        self.grid = grid
        self.shader = be
        self.uiManager = uiManager
        self.textureManager = textureManager;
    }
    
    
    
    func startGame() {
        // player1's turn
        if (setTurn(p: player1)) {
            startTurn()
        }
        
        createArmies();
        /*var temp : [Int]! = player1.getArmy();
        for index in 0 ... sizeOfArmies {
            var r : Int! = unitManager[temp[index]]?.attackRange
            print("Attack Range \(r)")
        }*/
        startTurn()
    }
    
    
    
    func endGame() {
        // Winner message, display score, switch views?
    }
    
    
    
     func startTurn() {
        // player selects unit, moves, attacks, updates score, etc.
    }

    
    
     func endTurn() -> Bool? {
        if (curPlayer.getPlayerID() == 1) {
            print("Player 2")
            return setTurn(p: player2)  // return true
            
        }
        else if (curPlayer.getPlayerID() == 2) {
            print("Player 1")
            return setTurn(p: player1)  // return true
            
        }
        return false
    }
    
    
    
     func setTurn(p: Player) -> Bool! {
        //MARK: Comparing Optional Values
//        guard p != nil else {
//                return false
//        }
        self.curPlayer = p
        uiManager.setPlayer(player: p.getPlayerID())
        return true
    }
    
    
    
    // make sure this returns > than 0
     func getTurn() -> Int! {
        return self.curPlayer.getPlayerID()
    }
    
    
    
    
    func updateScore() {
        // if enemy dies on attack
        // get the dead unit's ID
        // increment Player.setScore(Int) based off that unit type
        
        if (getTurn() == 1) {
            //player1.setScore(score: player1.getScore() + unitDB1.[killedID].getPointsWorth())
        }
        else if (getTurn() == 2) {
            //player2.setScore(score: player2.getScore() + unitDB2.[killedID].getPointsWorth())
        }
    }
    
    
    
    /*
     * Name: checkForWin(id : Int!) -> Void
     * Description:
     *   returns a bool for whether or not the player (id passed in) has won.
     */
    func checkForWin(playerID: Int) -> Bool {    // call this every enemy death?

        // Check size of the Player's army array
        //  if 0 game over
        return true
    }
    
    
    
    /*  THIS COULD BE IN ATTACK MANAGER??
     * Name: killUnit(playerID : Int) -> Void
     * Description:
     *   Depends on the AttackManager...If AM has a getKilledID() method returning the unitID of a dead unit,
     *   we can remove the unit from teh Player's army array with this function
     */
    func killUnit(playerID : Int) {
        // Should AttackManager check for death after attack?  e.g.  getKilledID() {}
        // If so, we'll have a killedID and use the following code:
        
        // Find killed unit inside Player class, remove it
        if (playerID == 1) {
            //player1.removeID(id : killedID)
        }
        else if (playerID == 2) {
            //player2.removeID(id : killedID)
        }
    }

     func checkUnit(unitID : Int) -> Bool? {
        return false
    }
    
    /*
     * Name: getScoreByID(playerID : Int) -> Int!
     * Description:
     *   returns the player's score (an Int) based on the player ID that's passed in.
     *  Warning: Will return NIL if non-valid playerID is passed in (e.g. < 1 or > 2)
     */
     func getScoreByID(playerID : Int) -> Int! {
        if (playerID == 1) {
            return player1.getScore()
        }
        else if (playerID == 2) {
            return player2.getScore()
        }
        return -1       // something went wrong
    }
    
    
    
    /*
     * Name: getUnitByID(id : Int) -> Unit!
     * Description:
     *   returns the coresponding unit object to the unitID that's passed in.
     *   Dependant on the current player (who's turn it is)
     * Warning: returns NIL if incorrect UnitID is passed in ( < 0 or > 14)
     */
     func getUnitByID(id : Int) -> Unit! {
        return unitManager[id]
       /* if (id >= 0 && id <= 14) {
            if (getTurn() == 1) {
                return unitDB1[id]   // returns a unit
            }
            else if (getTurn() == 2) {
                return unitDB2[id]   // returns a unit
            }
        }
        return nil  // watchout here. NIL*/
    }
    
    func createArmies() {
        addArmyToPlayer(p: player1);
        addArmyToPlayer(p: player2)
    }
    
    func addArmyToPlayer(p : Player) {
        var cell : Cell!
        var unit : Unit!
        var army : [Int]! = [];
        for index in 0...sizeOfArmies{

            if(index < 3) {
                unit = Tank(id : ids)
            } else {
                unit = Artillery(id : ids)
            }
            
            unitManager.updateValue(unit, forKey: ids)
            unitManager[ids]?.type()
            //sets unit to starting cell
            
            if(p.getPlayerID() == 1) {
                cell = grid.getCell(row: player1StartPos[index][0], column: player1StartPos[index][1])
                if(cell.setUnitID(newUnitID: ids)) {
                    unitManager[ids]?.type()
                    getUnitByID(id: ids).type()
                    setCellModel(cellTapped: cell, pid: p.getPlayerID());
                    army.append(ids)
                    ids += 1
                }
            } else {
                cell = grid.getCell(row: player2StartPos[index][0], column: player2StartPos[index][1])
                if(cell.setUnitID(newUnitID: ids)) {
                    setCellModel(cellTapped: cell, pid: p.getPlayerID());
                    army.append(ids)
                    ids += 1
                }
            }
        }
        p.setArmy(a: army)
    }
    
    func setCellModel(cellTapped : Cell, pid : Int) {
//        var model : Model?
        let newpos = cellTapped.getPosition()
        //let model: Model = Cube(shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -5.0, scale: 0.5)
        // load obj file named "key.obj"
        
        if (pid == 1) {

            //model.scale = 0.2
            //MARK: Optional Value
//            if (tank != nil) {
            print("type = \(getUnitByID(id: cellTapped.getUnitID()).type())")
            if(getUnitByID(id: cellTapped.getUnitID()).type() == 2) {
                let tank: Model = ObjModel(name: "tank", objstruct: textureManager.objTextures[0], shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -4.2, scale: 0.3)
                print("TANK MODEL INITIALIZED")
                //model.setColor(red: Settings.one_red, green: Settings.one_green, blue: Settings.one_blue)
                tank.setColor(red: 0.0, green: 0.0, blue: 1.0);
                tank.setRotation(x: GLKMathDegreesToRadians(90.0), y: GLKMathDegreesToRadians(90.0), z: 0);
                //model.setColor(red: Settings.two_red, green: Settings.two_green, blue: Settings.two_blue)
                cellTapped.setUnit(model : tank)
            } else {
                let artillery: Model = ObjModel(name: "Old_mortar", objstruct: textureManager.objTextures[2], shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -5.0, scale: 0.75)
                print("Artillery MODEL INITIALIZED")
                artillery.setColor(red: 0.0, green: 0.0, blue: 1.0);
                artillery.setRotation(x: GLKMathDegreesToRadians(90.0), y: GLKMathDegreesToRadians(0.0), z: 0);
                cellTapped.setUnit(model:artillery)
            }

            //let box: Model = ObjModel(name: "box", objstruct: textureManager.objTextures[2], shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -4.2, scale: 3.0)
                //print("Box MODEL INITIALIZED")
                //box.setColor(red: 0.0, green: 0.0, blue: 1.0);
                //box.setRotation(x: GLKMathDegreesToRadians(90.0), y: GLKMathDegreesToRadians(90.0), z: 0);
                //cellTapped.setUnit(model:box)
//            }
        }
        else {
            if(getUnitByID(id: cellTapped.getUnitID()).type() == 2) {
                let tank: Model = ObjModel(name: "tankR", objstruct: textureManager.objTextures[1], shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -4.2, scale: 0.3)
            //model.scale = 0.2
            //MARK: Optional Value
//            if (tank != nil) {
                print("TANK MODEL INITIALIZED")
                tank.setColor(red: 1.0, green: 0.0, blue: 0.0);
                tank.setRotation(x: GLKMathDegreesToRadians(90.0), y: GLKMathDegreesToRadians(-90.0), z: 0);
                cellTapped.setUnit(model : tank)
//            }
            }else{
                let artillery: Model = ObjModel(name: "Old_mortarR", objstruct: textureManager.objTextures[3], shader: self.shader, x: Float(newpos.x), y: Float(newpos.y), z: -5.0, scale: 0.75)
                print("Artillery MODEL INITIALIZED")
                artillery.setColor(red: 1.0, green: 0.0, blue: 0.0);
                artillery.setRotation(x: GLKMathDegreesToRadians(90.0), y: GLKMathDegreesToRadians(180.0), z: 0);
                cellTapped.setUnit(model:artillery)
            }
        }
    }
    
    func getMovePoints(id : Int) -> Int {
        var temp : Unit! = unitManager[id]
        return temp.movePointsCurrent
    }
    
    func getAttackRange(id : Int) -> Int {
        var temp : Unit! = unitManager[id]
        return temp.attackRange
    }
    
    func getUnitWithId(id : Int) -> Unit {
        return unitManager[id]!
    }
    
    func unitInPlayersArmy(id : Int) -> Bool{
        let army = curPlayer.getArmy()
        
        for i in army {
            if(id == i) {
                return true
            }
        }
        
        return false
    }
    
    func cellInMoveRange(cell : Cell) -> Bool{
        for c in movementCells {
            if(cell.getCol() == c.getCol() && cell.getRow() == c.getRow()){
                return true
            }
        }
        return false
    }
    
    func setMovementCells(cells : [Cell]) {
        movementCells = cells;
    }
    
    func setAttackCells(cells : [Cell]) {
        attackCells = cells;
    }
    
    func cellInAttackRange(cell : Cell) -> Bool {
        for c in attackCells {
            if(cell.getCol() == c.getCol() && cell.getRow() == c.getRow()){
                return true
            }
        }
        return false
    }
    
    func isEnemyUnit(id : Int) -> Bool {
        let uIDs : [Int]!
        if(curPlayer.getPlayerID() == 1) {
            uIDs = player2.getArmy()
        } else {
            uIDs = player1.getArmy()
        }
        
        for i in uIDs {
            if(i == id) {
                return true
            }
        }
        return false
    }
    
    func selectCell(cell : Cell) {
        curCell = cell
        uiManager.selectingUnit(unit: getUnitByID(id: curCell.getUnitID()))
    }
    
    func deselectCurCell() {
        curCell = nil
        uiManager.deselectUnit()
    }
    
    func resetArmy() {
        let army : [Int]! = curPlayer.getArmy()
        
        for u in army {
            getUnitByID(id: u).resetMove()
            getUnitByID(id: u).resetAttack()
        }
    }
    
    func attackUnit(cell : Cell) {
        
        if(getUnitByID(id: curCell.getUnitID()).hasAttack){
            
            let attacker : Unit! = getUnitByID(id: curCell.getUnitID())
            let victim : Unit! = getUnitByID(id: cell.getUnitID())
            attacker.type();
            print(attacker.attackDamage)
            attackManager.attackUnit(dmg: attacker.attackDamage, unit: victim)
            curPlayer.setScore(score: 1)
            if(victim.getHealth() <= 0) {
                curPlayer.kills = curPlayer.kills + 1
                if(curPlayer.kills >= sizeOfArmies + 1) {
                    //End game************************************
                    print("Game over");
                    finalScore = curPlayer.getScore()
                    gameOver = true
                }
            }
            getUnitByID(id: curCell.getUnitID()).hasAttacked()
            print(getUnitByID(id: curCell.getUnitID()).hasAttack)
            
        }
    }
    
    func unitIsAlive(id : Int)->Bool {
        let unit : Unit! = getUnitByID(id: id)
        
        if(unit.getHealth() > 0){
            return true;
        }
        
        print("\(id) : dead")
        return false
    }
    
    // Goes through cells within a radius of a row and coloumn changes their colours and returns an array of those cells
    func setMovementReticleColours(texture: GLuint, red: Float, green: Float, blue: Float, radius: Int, row : Int, col : Int, gridSystem : GridController) -> [Cell] {
        let grid = gridSystem.getGrid()
        var cells : [Cell]! = []
        var count : Int! = 0
        for i in 0 ..< grid.count {
            for j in 0 ..< grid[i].count {
                if(abs(i - row) + abs(j - col) <= radius) {
                    if(grid[i][j].getUnitID() < 0) {
                        grid[i][j].setColor(red: red, green: green, blue: blue)
                        //grid[i][j].setTexture(file: texture)
                        //MARK: Movement range set Colors
                        cells.append(grid[i][j])
                        count = count + 1
                    }
                }
            }
        }
        print("Number of cells added \(count)")
        return cells
    }
    
    //change cell color to red if enemy within it
    func setAttackReticleColours(texture: GLuint, red: Float, green: Float, blue: Float, radius: Int, row : Int, col : Int, gridSystem : GridController) -> [Cell] {
        let grid = gridSystem.getGrid()
        var cells : [Cell]! = []
        
        for i in 0 ..< grid.count {
            for j in 0 ..< grid[i].count {
                if(abs(i - row) <= radius && abs(j - col) <= radius) {
                    if(grid[i][j].getUnitID() > -1) {
                        if(self.isEnemyUnit(id : grid[i][j].getUnitID())) {
                            grid[i][j].setColor(red: red, green: green, blue: blue)
                            //grid[i][j].setTexture(file: texture)
                            //MARK: Settings Attack Colors
                            cells.append(grid[i][j])
                        }
                    }
                }
            }
        }
        return cells
    }
    
    func resetReticleColors() {
//        var count : Int! = 0
        for x in movementCells {
            //x.setTexture(file: textureManager.textures[0])
            x.setColor(red: 20/256, green: 117/256, blue: 5/256)
        }
        
        for i in 0 ..< attackCells.count {
            //attackCells[i].setTexture(file: textureManager.textures[0])
            attackCells[i].setColor(red: 20/256, green: 117/256, blue: 5/256)
        }
    }
    
    func hasAttackCells() -> Bool{
        if(attackCells != nil) {
            return true;
        }
        
        return false;
    }
    
}
