//
//  TextureManager.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-04-05.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import GLKit
import Foundation

class TextureManager {
    var textures : Array<GLuint> = Array()
    private var filenames : Array<String> = [
        "grass.jpg",
        "sand.png",
        "brick.png",
        "blockDiamond.png",
        "blockGold.png"
    ]
    
    var objTextures : Array<ObjStruct> = Array()
    private var objFilenames : Array<String> = [
        "tank",
        "tankR",
        "Old_mortar",
        "Old_mortarR",
        "box"
    ]
    
    init() {
        self.loadTextures()
        self.loadObjTextures()
    }
    
    private func textureLoader(file: String) -> GLuint {
        let path = Bundle.main.path(forResource: file, ofType: nil)!
        let option = [GLKTextureLoaderOriginBottomLeft: true]
        do {
            let info = try GLKTextureLoader.texture(withContentsOfFile: path, options: option as [String : NSNumber]?)
            return info.name
        } catch {
            NSLog("Failed to load model texture \(file)")
        }
        return (0) as GLuint
    }
    
    private func loadTextures() {
        var count = 0
        for file in filenames {
            self.textures.insert(self.textureLoader(file: file), at: count)
            count += 1;
        }
        print(filenames[4])
    }
    
    private func loadObjTextures() {
        for file in objFilenames {
            let objstruct : ObjStruct = ObjStruct()
            let fixtureHelper = FixtureHelper()
            let source = try? fixtureHelper.loadObjFixture(name: file)
            if let source = source {
                let loader = ObjLoader(source: source, basePath: fixtureHelper.resourcePath)
                
                do {
                    let shapes = try loader.read()
                    let tmp = (shapes.first!.material?.diffuseTextureMapFilePath?.lastPathComponent)! as String
//                    let tmp2 = (tmp.material?.diffuseTextureMapFilePath?.lastPathComponent)! as String
//                    self.objTextures.append(self.textureLoader(file: tmp))
                    objstruct.texture = self.textureLoader(file: tmp)
                    
                    for vertexIndexes in shapes.first!.faces {
                        for vertexIndex in vertexIndexes {
                            // Will cause an out of bounds error
                            // if vIndex, nIndex or tIndex is not normalized
                            // to be local to the internal data of the shape
                            // instead of global to the file as per the
                            // .obj specification
                            let (vertexVector, normalVector, textureVector) = shapes.first!.dataForVertexIndex(vertexIndex)
                            
                            var v = Vertex()
                            if let vertexVector = vertexVector {
                                v.x = GLfloat(vertexVector[0])
                                v.y = GLfloat(vertexVector[1])
                                v.z = GLfloat(vertexVector[2])
                            }
                            
                            if let normalVector = normalVector {
                                v.nx = GLfloat(normalVector[0])
                                v.ny = GLfloat(normalVector[1])
                                v.nz = GLfloat(normalVector[2])
                            }
                            
                            if let textureVector = textureVector {
                                v.u = GLfloat(textureVector[0])
                                v.v = GLfloat(textureVector[1])
                            }
                            
//                            vertices.append(v) //var vertices: [Vertex] = []
                            objstruct.vertices.append(v)
//                            indices.append(index) //var indices: [GLuint] = []
                            objstruct.indices.append(objstruct.index)
//                            index = (index + 1 as GLuint) //var index: GLuint = 0
                            objstruct.index = (objstruct.index + 1 as GLuint)
                        }
                    }
                    self.objTextures.append(objstruct)
                } catch {
                    NSLog("Parsing failed with unknown error")
                }
            } else {
                NSLog("obj not found")
            }
        }
    }
}
