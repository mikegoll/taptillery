//
//  UIManager.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-03-26.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import UIKit

class UIManager {
    private var playerTag: UILabel
    
    //UnitView
    private var unityTypeTag: UILabel
    private var healthTag: UILabel
    private var damageTag: UILabel
    
    private var generalView: UIView
    private var unitView: UIView
    
    private var unitSelected: Bool
    
    private var unit: Unit?
    
    init(tags: [UILabel], views: [UIView]) {
        //Unit
        self.playerTag = tags[0]
        self.unityTypeTag = tags[1]
        self.healthTag = tags[2]
        self.damageTag = tags[3]
        
        //Views
        self.generalView = views[0]
        self.unitView = views[1]
        
        self.unitSelected = false
        self.unitView.isHidden = true
    }
    
    func setPlayer(player: Int) {
        self.playerTag.text = "Player \(player)"
        deselectUnit()
    }
    
    func selectingUnit(unit: Unit) {
        self.unitView.isHidden = false
        self.unitSelected = true
        self.unit = unit
        setUnit()
    }
    
    private func selectingNon() {
        //OUT OF SCOPE
    }
    
    func deselectUnit() {
        self.unitView.isHidden = true
        self.unitSelected = false
        self.unit = nil
    }
    
    private func setUnit() {
        if (self.unit != nil) {
            setHealth(totalHealth: self.unit!.healthTotal, currentHealth: self.unit!.healthCurrent)
            setDamage(damage: self.unit!.attackDamage)
        }
    }
    
    private func setHealth (totalHealth: Int, currentHealth: Int) {
        self.healthTag.text = "Health: \(currentHealth) / \(totalHealth)"
    }
    
//    private func setMoveVarable(totalPoints: Int, movePoints: Int) {
//        self.moveTag.text = "Move Points: \(movePoints) / \(totalPoints)"
//    }
    
//    private func setAttack(hasAttack: Bool) {
//        self.attackTag.text = "Has Attack: \(hasAttack)"
//    }
    
    private func setDamage(damage: Int) {
        self.damageTag.text = "Damage: \(damage)"
    }
    
//    func selectingEnemy(enemy: Unit) {
//        //Add if for during animations
//        if (unitSelected) {
//            self.attackView.isHidden = false
//            setEnemyHealth(totalHealth: enemy.healthTotal, currentHealth: enemy.healthCurrent)
//            setEnemyDamage(unitAttackDamage: self.unit!.attackDamage, currentHealth: enemy.healthCurrent)
//        }
//    }
    
//    private func setEnemyHealth (totalHealth: Int, currentHealth: Int) {
//        self.enemmyHealthTag.text = "Health: \(currentHealth) / \(totalHealth)"
//    }
    
//    private func setEnemyDamage (unitAttackDamage: Int, currentHealth: Int) {
//        if (unitAttackDamage >= currentHealth) {
//            self.enemyDamageTag.text = "Damage: \(currentHealth)"
//        } else {
//            self.enemyDamageTag.text = "Damage: \(unitAttackDamage)"
//        }
//    }
}
