//
//  Settings.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-01-17.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation

struct Settings {
    static var attack : Int = 60
    
    //MARK: Cell Settings
    static let row : Float = 7.0
    static let column : Float = 13.0
    static let spacing : Float = 2.7
    
    //Player One
    static let one_red : Float = (250.0 / 255)
    static let one_green : Float = (0.0 / 255)
    static let one_blue : Float = (255.0 / 255)
    
    //Player two
    static let two_red : Float = (255.0 / 255)
    static let two_green : Float = (199.0 / 255)
    static let two_blue : Float = (0.0 / 255)
}
