//
//  AttackManager.swift
//  Taptillery
//
//  Created by Maitiu Morton on 2018-02-18.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation

class AttackManager {
    
    init() {
        
    }
    //function takes in a damage amount and a unit then Runs unit's take damage funciton
    //and passing in the damage dealt
    func attackUnit(dmg: Int, unit : Unit) {
        print("Attacked: \(unit.getHealth())")
        unit.takeDmg(x: dmg)
    }
    
}
