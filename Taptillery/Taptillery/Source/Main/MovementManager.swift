//
//  MovementManager.swift
//  Taptillery
//
//  Created by Maitiu Morton on 2018-02-18.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation

class MovementManager {
    
    init() {
        
    }
    //function takes in a Unit Matrix and translates it to position
    //and passing in the damage dealt
    func MoveUnit(xCoord: Double, zCood: Double) {
        //while still not at X coord
        MoveAlongX();
        
        //while still not at yCoord
        MoveAlongZ();
        
    }
    
    //takes in martix moves it along x axis
    func MoveAlongX() {
        
    }
    
    //takes in matrix moves it along z axis
    func MoveAlongZ() {
        
    }
    
}
