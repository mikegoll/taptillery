//
//  Emitter.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-04-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit


struct Emitter {
    let NUM_PARTICLES = 180
    var eParticles : [Particles?] = []
    var ePosition : GLKVector2 = GLKVector2Make(0.0, 0.0)
    var eRadius : GLfloat = 0.0
    var eVelocity : GLfloat = 0.0
    var eDecay : GLfloat = 0.0
    var eSizeStart : GLfloat = 0.0
    var eSizeEnd : GLfloat = 0.0
    var eColorStart : GLKVector3 = GLKVector3Make(0.0, 0.0, 0.0)
    var eColorEnd : GLKVector3 = GLKVector3Make(0.0, 0.0, 0.0)
    
    
    init() {
        self.eParticles = [Particles?](repeating: nil, count: NUM_PARTICLES)
    }
}
