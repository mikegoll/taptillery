//
//  Particles.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-04-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

struct Particles {
    var pID : GLfloat = 0.0
    var pRadiusOffset : GLfloat = 0.0
    var pVelocityOffset : GLfloat = 0.0
    var pDecayOffset : GLfloat = 0.0
    var pSizeOffset : GLfloat = 0.0
    var pColorOffset : GLKVector3 = GLKVector3Make(0.0, 0.0, 0.0)
    
    
    init() {
    }
}

