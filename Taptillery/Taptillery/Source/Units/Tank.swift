//
//  Tank.swift
//  taptillery
//
//  Created by Simon on 2/18/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

class Tank: Unit {
    
    var healthTotal: Int = 15
    
    var healthCurrent: Int = 15
    
    var attackDamage: Int = 5
    
    var attackRange: Int = 2
    
    var hasAttack: Bool = true
    
    var movePointsTotal: Int = 3
    
    var movePointsCurrent: Int = 3
    
    var pointsWorth : Int = 500
    
    var unitID: Int
    
    var currentCell: [Int]! = [0,0]
    var moved : Bool! = false
    
    var attacked: Bool! = false
    
    init(id: Int){
        self.unitID = id
    }
    
    deinit {
    
    }
    
    func die() {
        
    }
    
    func moveUnit() {
        
    }
    
    func attack(unit: Unit) {
        
    }
    
    func resetMove() {
        moved = false
    }
    
    func hasMoved() {
        moved = true
    }
    
    func getHealth() -> Int {
        return healthCurrent
    }
    
    func takeDmg(x: Int) {
        healthCurrent -= x
    }
    
    func hasAttacked() {
        hasAttack = false
    }
    
    func resetAttack() {
        hasAttack = true
    }
    
    func type() -> Int{
        print("Tank")
        return 2
    }
}
