//
//  Unit.swift
//  taptillery
//
//  Created by Simon on 2/18/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

protocol Unit {
    
    var healthTotal: Int {get set};
    var healthCurrent: Int {get set};
    var attackDamage: Int {get set};
    var attackRange: Int {get set};
    var hasAttack: Bool {get set};
    var movePointsTotal: Int {get set};
    var movePointsCurrent: Int {get set};
    var pointsWorth : Int {get set};
    var unitID: Int {get set};
    var currentCell : [Int]! {get set};
    var moved : Bool! {get};
    
    
    //Kills unit and deletes from the game.
    func die();
    
    //Moves the unit
    func moveUnit();
    
    //Attacks
    func attack(unit: Unit);
    
    func resetMove()
    
    func hasMoved()
    
    func getHealth() -> Int
    
    func takeDmg(x : Int)
    
    func hasAttacked()
    func resetAttack()
    
    func type() -> Int
}

extension Unit{
    
    //Decreases movementPointsCurrent by 1.
    mutating func movementDecrement() {
        self.movePointsCurrent -= 1;
    }
    
    //Resets movementPointsCurrent to the movementPointsTotal value.
    mutating func movementReset() {
        self.movePointsCurrent = self.movePointsTotal;
    }
    
    //Reduces the healthCurrent variable;
    mutating func healthDamage(damage: Int) {
        self.healthCurrent = self.healthCurrent - damage;
        
        if(self.healthCurrent < 0){
            die();
        }
        
    }
}

