//
//  Infantry.swift
//  taptillery
//
//  Created by Simon on 2/18/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

class Infantry: Unit {
    
    var healthTotal: Int = 10
    
    var healthCurrent: Int = 10
    
    var attackDamage: Int = 2
    
    var attackRange: Int = 2
    
    var hasAttack: Bool = true
    
    var movePointsTotal: Int = 2
    
    var movePointsCurrent: Int = 2
    
    var pointsWorth : Int = 100
    
    var unitID: Int
    
    var currentCell: [Int]! = [0,0]
    
    var moved : Bool! = false
    
    init(id: Int!){
        self.unitID = id
    }
    
    func die() {
        
    }
    
    func movementDecrement() {
        
    }
    
    func movementReset() {
        
    }
    
    func healthDamage(damage: Int) {
        
    }
    
    func moveUnit() {
        
    }
    
    func attack(unit: Unit) {
        
    }
    
    func resetMove() {
        moved = false
    }
    
    func hasMoved() {
        moved = true
    }
    
    func getHealth() -> Int {
        return healthCurrent
    }
    
    func takeDmg(x: Int) {
        healthCurrent -= x
    }
    
    func hasAttacked() {
        hasAttack = false
    }
    
    func resetAttack() {
        hasAttack = true
    }
    
    func type() -> Int{
        print("Infantry")
        return 1;
    }
}
