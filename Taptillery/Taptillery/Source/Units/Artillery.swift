//
//  Artillery.swift
//  taptillery
//
//  Created by Simon on 2/18/18.
//  Copyright © 2018 Simon. All rights reserved.
//

import Foundation

class Artillery: Unit{
    
    var healthTotal: Int = 5
    
    var healthCurrent: Int = 5
    
    var attackDamage: Int = 2
    
    var attackRange: Int = 5
    
    var hasAttack: Bool = true
    
    var movePointsTotal: Int = 1
    
    var movePointsCurrent: Int = 1
    
    var pointsWorth : Int = 250
    
    var unitID: Int
    var currentCell: [Int]! = [0,0]
    
    var moved : Bool! = false
    
    
    init(id: Int){
        self.unitID = id
    }
    
    func die() {
        
    }
    
    func movementDecrement() {
        
    }
    
    func movementReset() {
        
    }
    
    func healthDamage(damage: Int) {
        
    }
    
    func moveUnit() {
        
    }
    
    func attack(unit: Unit) {
        
    }
    
    func resetMove() {
        moved = false
    }
    
    func hasMoved() {
        moved = true
    }
    
    func getHealth() -> Int {
        return healthCurrent
    }
    
    func takeDmg(x: Int) {
        healthCurrent -= x
    }
    
    func hasAttacked() {
        hasAttack = false
    }
    
    func resetAttack() {
        hasAttack = true
    }
    
    func type() -> Int {
        print("Artillery")
        return 3
    }
}
