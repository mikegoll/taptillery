//
//  Player.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-02-19.
//  Copyright � 2018 DigitalMob. All rights reserved.
//

import Foundation

public class Player {
    private var selectedUnit: Int!     // Current selected Unit
    // Army.  Dictonary because ID's will be the same as the indexes.  (no brute force finding later)
    private var army : [Int] = [];
    
    private var playerID: Int?   // player turn, can be 1 or 2
    private var score : Int! = 0;
    public var kills : Int! = 0;
    
    //MARK: Can use "= [Tank]()
    //MARK: default is "public" should this be private with getters and setters?
    /*private var tank = Array(repeating: Tank(playerID: 0), count: 3)
    // tank is of type [Tank], and equals [Tank(), Tank(), Tank()]
    
    private var artillery = Array(repeating: Artillery(), count: 6)
    // artillery is of type [Artillery], and equals [Artillery(), Artillery(), Artillery()... x2]
    
    private var infantry = Array(repeating: Infantry(), count: 9)*/
    // infantry is of type [Infantry], and equals [Infantry(), Infantry(), Infantry(),...x3]
    
    
    // Constructor
    init(id : Int) {
        self.setPlayerID(pid: id)
        // this is our army array
    }
    
    
    func setArmy(a : [Int]) {
        army = a;
    }
    
    /*
     * Name: setPlayerID()
     * Description:
     *  sets the player ID to the provided int.  Can be 1 or 2
     */
    func setPlayerID(pid: Int){
        
        if(pid >= 1 && pid <= 2) {
            playerID = pid
        }
    }
    
    
    /*
     * Name: getPlayerID()
     * Description:
     *  returns the player's ID
    */
    func getPlayerID() -> Int! {
        return self.playerID!
    }
    
    
    /*
     * Name: getArmy()
     * Description:
     *   returns the player's army, an array of Unit(s)
     */
    func getArmy() -> [Int] {
        return self.army
    }
    
    
    /*
     * Name: currentlySelectedUnit()
     * Description:
     *   returns the player's current selected Unit.
     *   make sure to check for nil when using this.
     */
    func currentlySelectedUnit() -> Unit? {
        return self.selectedUnit as? Unit
    }
    
    
    /*
     * Name: selectUnit(unit:Unit) -> Void
     * Description:
     *   Selects the unit that is passed in for this player.
     */
    func selectUnit(id : Int!) -> Bool {
        guard id >= 0 else {
            return false
        }
        self.selectedUnit = id
        return true
    }
    
    
    /*
     * Name: unSelectUnit() -> Void
     * Description:
     *   UnSelects the current selected unit, making it -1.
     */
    public func unSelectUnit() {
        self.selectedUnit = -1
    }
    
    
    
    /*
     * Name: setScore(Int) -> Void
     * Description:
     *   set this player's score .
     */
    func setScore(score : Int!) {
        self.score = self.score + score
    }
    
    
    
    /*
     * Name: getScore()
     * Description:
     *   returns the player's score (an Int).
     */
    func getScore() -> Int! {
        return self.score
    }
    
    
    
    /*
     * Name: removeID()
     * Description:
     *   removes / "kills" a unit based off the ID passed in
     */
    func removeID(id: Int) {
        army[id] = -1;
    }
    
    
    
    /*
     * Name: getArmySize()
     * Description:
     *   returns the size of this player's army
     */
    func getArmySize() -> Int! {
        return self.army.count
    }
    
}
