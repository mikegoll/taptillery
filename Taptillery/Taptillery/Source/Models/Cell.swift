//
//  Cell.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-02-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

class Cell : Model {
    private var border = false
    private var unitID : Int = -1
    var row : Int = 0
    var column : Int = 0
    var model : Model!
    var unitModel : Model!
    var cellT : Float = 1.0;
    //MARK: Initializer
    init(shader: BaseEffect) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        self.type = cellT;
    }
    
    init(shader: BaseEffect, x: Float, y: Float, z: Float) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        self.position.x = x
        self.position.y = y
        self.position.z = z
        self.type = cellT;
    }
    
    init(shader: BaseEffect, x: Float, y: Float, z: Float, scale: Float) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        self.position.x = x
        self.position.y = y
        self.position.z = z
        self.scale = scale
        self.type = cellT;
    }
    
    init(shader: BaseEffect, x: Float, y: Float, z: Float, scale: Float, row: Int, column: Int) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        self.position.x = x
        self.position.y = y
        self.position.z = z
        self.scale = scale
        self.row = row
        self.column = column
        self.r = 20/256
        self.g = 117/256
        self.b = 5/256
        self.type = cellT;
    }
    
    init(shader: BaseEffect, x: Float, y: Float, z: Float, scale: Float, red: Float, blue : Float, green: Float) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
        self.position.x = x
        self.position.y = y
        self.position.z = z
        self.scale = scale
        self.r = red
        self.g = green
        self.b = blue
        self.type = 1;
    }
    
    var vertexList : [Vertex] = [
        //       x     y   z   r    g    b    a    u    v  nx ny nz
        Vertex( 1.0, -1.0, 0, 1.0, 0.0, 0.0, 1.0, 0.25, 0, 0, 0, 1),
        Vertex( 1.0,  1.0, 0, 0.0, 1.0, 0.0, 1.0, 0.25, 0.25, 0, 0, 1),
        Vertex(-1.0,  1.0, 0, 0.0, 0.0, 1.0, 1.0, 0, 0.25, 0, 0, 1),
        Vertex(-1.0, -1.0, 0, 1.0, 1.0, 0.0, 1.0, 0, 0, 0, 0, 1)
    ]
    
    let indexList : [GLuint] = [
        0, 1, 2,
        2, 3, 0
    ]

    //MARK: Called every time delta (Update)
    override func updateWithDelta(_ dt: TimeInterval) {
//        let secsPerMove = 2.0
//        self.position = GLKVector3Make(
//            Float(sin(CACurrentMediaTime() * 2 * Double.pi / secsPerMove)),
//            self.position.y,
//            self.position.z)
    }
    
    override func renderWithParentMoelViewMatrix(_ parentModelViewMatrix: GLKMatrix4) {
        //glUniform4f(self.shader.colorUniform, GLfloat(r), GLfloat(g), GLfloat(b), 1)
        super.renderWithParentMoelViewMatrix(parentModelViewMatrix)
    }
    
    override func getModelInfo() {
        super.getModelInfo()
        print("Row: \(self.row) - Column: \(self.column)")
        print("Border: \(self.border)")
    }
    
    //MARK: other funcitons
    func setBorderVal(border: Bool) {
        if (border) {
            super.setColor()
        } else {
            super.setColor(red: 20/256, green: 117/256, blue: 5/256)
        }
        self.border = border
    }
    
    func IsBorder() -> Bool {
        return self.border
    }
    
    func getPosition() -> GLKVector3 {
//        if (self.column == 0) {
//            let tmprow = Float(-Int(Settings.row/2) + row) * Settings.spacing
//            let tmpcol = Float(-Int(Settings.column/2)) * Settings.spacing
//            return GLKVector3(v: (tmpcol, tmprow, -5.0))
//        }
        return self.position
    }
    
    func setUnitID(newUnitID: Int) -> Bool {
        if (self.border) {
            return false
        }
        self.unitID = newUnitID
        return true
    }
    
    //MARK: Returns -1 if the cell doesn't contain a unit
    func getUnitID() -> Int {
        if (self.border) {
            return -1
        }
        return self.unitID
    }
    
    func setUnit(model : Model){
        unitModel = model
    }
    
    func getUnit() -> Model{
        return unitModel
    }
    
    func getCol() -> Int {
        return column
    }
    
    func getRow() -> Int {
        return row
    }
    
//    func findCellUnit() -> Unit {
//        //MARK: Change when the Unit HashMap is finished!
//        //let unit : Unit = HashMap.get(self.unitID)!
//        //return
//    }
}
