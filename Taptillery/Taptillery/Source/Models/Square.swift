//
//  Square.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-02-13.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import GLKit

class Square : Model {
    
    let vertexList : [Vertex] = [
        //       x     y   z   r    g    b    a    u    v  nx ny nz
        Vertex( 1.0, -1.0, 0, 1.0, 0.0, 0.0, 1.0, 0.25, 0, 0, 0, 1),
        Vertex( 1.0,  1.0, 0, 0.0, 1.0, 0.0, 1.0, 0.25, 0.25, 0, 0, 1),
        Vertex(-1.0,  1.0, 0, 0.0, 0.0, 1.0, 1.0, 0, 0.25, 0, 0, 1),
        Vertex(-1.0, -1.0, 0, 1.0, 1.0, 0.0, 1.0, 0, 0, 0, 0, 1)
    ]
    
    let indexList : [GLuint] = [
        0, 1, 2,
        2, 3, 0
    ]
    
    init(shader: BaseEffect) {
        super.init(name: "square", shader: shader, vertices: vertexList, indices: indexList)
    }
    
    override func updateWithDelta(_ dt: TimeInterval) {
        let secsPerMove = 2.0
        self.position = GLKVector3Make(
            Float(sin(CACurrentMediaTime() * 2 * Double.pi / secsPerMove)),
            self.position.y,
            self.position.z)
    }
}
