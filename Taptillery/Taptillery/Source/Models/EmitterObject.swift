//
//  EmitterObject.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-04-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

class EmitterObject {
    var shader: EmitterShader!
    var emitter : Emitter?
    var texture: GLuint = 0
    var _particleBuffer : GLuint
    var _gravity : GLKVector2
    var _life : Float
    var _time : Float
    var size : Int
    var count : Int = 0
    let NUM_PARTICLES : Int = 180
    
    init(texture: String, position: GLKVector2) {
        // Initialize variables
        _particleBuffer = 0;
        _gravity = GLKVector2Make(0.0, 0.0)
        _life = 0.0
        _time = 0.0
        
        self.size =  MemoryLayout<Particles>.size
        //self.shader = shader
        self.loadShader()
        self.loadTexture(texture)
        self.loadParticleSystem(position: position)
        
        //super.init(name: "EmitterObject", shader: shader, texture: texture)
        
    }
    

    // LOAD TEXTURE
    func loadTexture(_ filename: String) {
        
        let path = Bundle.main.path(forResource: filename, ofType: nil)!
        let option = [ GLKTextureLoaderOriginBottomLeft: true]
        do {
            let info = try GLKTextureLoader.texture(withContentsOfFile: path, options: option as [String : NSNumber]?)
            self.texture = info.name
        } catch {
            NSLog("Failed to load particle texture");
        }
        
        glBindTexture(GLenum(GL_TEXTURE_2D), self.texture)
    }
    
    
    
    func loadShader() {
        self.shader = EmitterShader()
        self.shader.loadShader()
        glUseProgram(self.shader.program)
    }
    
    
    
    // RANDOM NUMBER GENERATOR
    func randomFloatBetween(min: Float, max: Float) -> Float {
        return Float(Float(arc4random()) / Float(UINT32_MAX)) * (max - min) + min
    }
    
    
    // LOAD PARTICLE SYSTEM
    func loadParticleSystem(position: GLKVector2) {
        var newEmitter : Emitter =  Emitter()
        
        
        // Offset bounds
        let oRadius : Float = 0.10      // 0.0 = circle; 1.0 = ring
        let oVelocity : Float = 0.50    // Speed
        let oDecay : Float = 0.25       // Time
        let oSize : Float = 8.00        // Pixels
        let oColor : Float = 0.25       // 0.5 = 50% shade offset
        
        for i in 0 ..< newEmitter.eParticles.count {
            // Assign a unique ID to each particle, between 0 and 360 (in radians)
            newEmitter.eParticles[i]?.pID = GLKMathDegreesToRadians(( Float(i) / Float(NUM_PARTICLES)) * 360.0)
            
            // Assign random offsets within bounds
            newEmitter.eParticles[i]?.pRadiusOffset = self.randomFloatBetween(min: oRadius, max: 1.00)
            newEmitter.eParticles[i]?.pVelocityOffset = self.randomFloatBetween(min: -oVelocity, max: oVelocity)
            newEmitter.eParticles[i]?.pDecayOffset = self.randomFloatBetween(min: -oDecay, max: oDecay)
            newEmitter.eParticles[i]?.pSizeOffset = self.randomFloatBetween(min: -oSize, max: oSize)
            let r = self.randomFloatBetween(min: -oColor, max: oColor)
            let g = self.randomFloatBetween(min: -oColor, max: oColor)
            let b = self.randomFloatBetween(min: -oColor, max: oColor)
            newEmitter.eParticles[i]?.pColorOffset = GLKVector3Make(r, g, b)
        }
        
        // Load Properties
        newEmitter.ePosition = position                               // Source position
        newEmitter.eRadius = 0.40                                     // Blast radius
        newEmitter.eVelocity = 3.00                                   // Explosion velocity
        newEmitter.eDecay = 2.00                                      // Explosion decay
        newEmitter.eSizeStart = 32.00                                 // Fragment start size
        newEmitter.eSizeEnd = 8.00                                    // Fragment end size
        newEmitter.eColorStart = GLKVector3Make(1.00, 0.50, 0.00)     // Fragment start color
        newEmitter.eColorEnd = GLKVector3Make(0.25, 0.00, 0.00)       // Fragment end color
        
        // Set global factors
        let growth : Float = newEmitter.eRadius / newEmitter.eVelocity  // Growth time
        _life = growth + newEmitter.eDecay + oDecay                     // Simulation lifetime
        
        let drag : Float = 10.00;                                       // Drag (air resistance)
        _gravity = GLKVector2Make(0.00, -9.81 * (1.0 / drag));          // World gravity
        
        self.count = newEmitter.eParticles.count
        
        // Set Emitter & VBO
        self.emitter = newEmitter;
        glGenBuffers(1, &_particleBuffer);
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), _particleBuffer);
        glBufferData(GLenum(GL_ARRAY_BUFFER), count * size, self.emitter?.eParticles, GLenum(GL_STATIC_DRAW));
    }
    
    
    
    
    func renderWithProjection(projectionMatrix: GLKMatrix4) {
        // Switch Buffers
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), _particleBuffer)
        
        // Uniforms
        glUniformMatrix4fv(self.shader.u_ProjectionMatrix, 1, GLboolean(GL_FALSE), projectionMatrix.array)
        glUniform2f(self.shader.u_Gravity, _gravity.x, _gravity.y)
        glUniform1f(self.shader.u_Time, _time)
        glUniform2f(self.shader.u_ePosition, self.emitter!.ePosition.x, self.emitter!.ePosition.y)
        glUniform1f(self.shader.u_eRadius, self.emitter!.eRadius)
        glUniform1f(self.shader.u_eVelocity, self.emitter!.eVelocity)
        glUniform1f(self.shader.u_eDecay, self.emitter!.eDecay)
        glUniform1f(self.shader.u_eSizeStart, self.emitter!.eSizeStart)
        glUniform1f(self.shader.u_eSizeEnd, self.emitter!.eSizeEnd)
        glUniform3f(self.shader.u_eColorStart, self.emitter!.eColorStart.r, self.emitter!.eColorStart.g, self.emitter!.eColorStart.b)
        glUniform3f(self.shader.u_eColorEnd, self.emitter!.eColorEnd.r, self.emitter!.eColorEnd.g, self.emitter!.eColorEnd.b)
        glUniform1i(self.shader.u_Texture, 0);
        
        // Attributes
        glEnableVertexAttribArray(GLuint(self.shader.a_pID))
        glEnableVertexAttribArray(GLuint(self.shader.a_pRadiusOffset))
        glEnableVertexAttribArray(GLuint(self.shader.a_pVelocityOffset))
        glEnableVertexAttribArray(GLuint(self.shader.a_pDecayOffset))
        glEnableVertexAttribArray(GLuint(self.shader.a_pSizeOffset))
        glEnableVertexAttribArray(GLuint(self.shader.a_pColorOffset))
        
        glVertexAttribPointer(GLuint(self.shader.a_pID), 1, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET(0))
        
        glVertexAttribPointer(GLuint(self.shader.a_pRadiusOffset), 1, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET((1) * MemoryLayout<GLfloat>.size))
            
        glVertexAttribPointer(GLuint(self.shader.a_pVelocityOffset), 1, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET((2) * MemoryLayout<GLfloat>.size))
            
        glVertexAttribPointer(GLuint(self.shader.a_pDecayOffset), 1, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET((3) * MemoryLayout<GLfloat>.size))
            
        glVertexAttribPointer(GLuint(self.shader.a_pSizeOffset), 1, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET((4) * MemoryLayout<GLfloat>.size))
            
        glVertexAttribPointer(GLuint(self.shader.a_pColorOffset), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), GLsizei(MemoryLayout<Particles>.size), BUFFER_OFFSET((5) * MemoryLayout<GLfloat>.size))
        
        
        // Draw particles
        glDrawArrays(GLenum(GL_POINTS), 0, GLsizei(NUM_PARTICLES))
        glDisableVertexAttribArray(GLuint(self.shader.a_pID))
        glDisableVertexAttribArray(GLuint(self.shader.a_pRadiusOffset))
        glDisableVertexAttribArray(GLuint(self.shader.a_pVelocityOffset))
        glDisableVertexAttribArray(GLuint(self.shader.a_pDecayOffset))
        glDisableVertexAttribArray(GLuint(self.shader.a_pSizeOffset))
        glDisableVertexAttribArray(GLuint(self.shader.a_pColorOffset))
    }
    
    
    
    func BUFFER_OFFSET(_ n: Int) -> UnsafeRawPointer? {
        return UnsafeRawPointer(bitPattern: n)
    }
    
    
    func updateWithDelta(_ dt: Float) -> Bool {
        _time += dt
        
        if(_time < _life) {
            return true
        }
        else {
            return false
        }
    }
}
