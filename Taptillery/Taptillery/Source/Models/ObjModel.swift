//
//  ObjModel.swift
//  ObjModelLoader
//
//  Created by Zac Koop.
//

import Foundation
import GLKit

class ObjModel: Model {
    
    init(name: String, objstruct: ObjStruct, shader: BaseEffect, x: Float, y: Float, z: Float, scale: Float) {
        
        var vertices: [Vertex] = []
        var indices: [GLuint] = []
        var index: GLuint = 0
        
        //assert(shape.vertices.count == shape.normals.count && shape.normals.count == shape.textureCoords.count, "wrong number of vertices, normals and texture coords")

        vertices = objstruct.vertices
        indices = objstruct.indices
        index = objstruct.index
        
        super.init(name: name, shader: shader, vertices: vertices, indices: indices)
        self.setTexture(file: objstruct.texture)

        self.position.x = x
        self.position.y = y
        self.position.z = z
        self.scale = scale
    }
    
    
    init(name: String, objstruct: ObjStruct, shader: BaseEffect) {
        
        var vertices: [Vertex] = []
        var indices: [GLuint] = []
        var index: GLuint = 0
        
        //assert(shape.vertices.count == shape.normals.count && shape.normals.count == shape.textureCoords.count, "wrong number of vertices, normals and texture coords")

        vertices = objstruct.vertices
        indices = objstruct.indices
        index = objstruct.index
        
        super.init(name: name, shader: shader, vertices: vertices, indices: indices)
        self.setTexture(file: objstruct.texture)
        self.rotationY = self.rotationY - Float(Double.pi * 0.6 / 8)
        self.rotationX = self.rotationX - Float(Double.pi * 0.6 / 8)
    }
    
    override func updateWithDelta(_ dt: TimeInterval) {
        self.rotationX = self.rotationX + Float(Double.pi * dt / 8)
    }
}
