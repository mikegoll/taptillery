//
//  BaseEffect.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-02-13.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

class BaseEffect {
    var programHandle : GLuint = 0
    var modelViewMatrixUniform : Int32 = 0
    var projectionMatrixUniform : Int32 = 0
    var textureUniform : Int32 = 0
    var colorUniform : Int32 = 0
    var surfaceColor : Int32 = 0
    var lightAmbientIntensityUniform : Int32 = 0
    var lightDiffuseIntensityUniform : Int32 = 0
    var lightDirectionUniform : Int32 = 0
    var lightSpecularIntensityUniform : Int32 = 0
    var lightShininessUniform : Int32 = 0
    
    var modelViewMatrix : GLKMatrix4 = GLKMatrix4Identity
    var projectionMatrix : GLKMatrix4 = GLKMatrix4Identity
    var texture: GLuint = 0
    var uType: Int32 = 0
    var type: Float = 0.0
    
    //Particle Emitter Attributes
    var a_pRadiusOffset : Int32 = 0
    var a_pVelocityOffset : Int32 = 0
    var a_pDecayOffset : Int32 = 0
    var a_pSizeOffset : Int32 = 0
    var a_pColorOffset : Int32 = 0
    
    // Particle Emitter Uniforms
    var u_ProjectionMatrix : Int32 = 0
    var u_Gravity : Int32 = 0
    var u_Time : Int32 = 0
    var u_Texture : Int32 = 0
    var u_ePosition : Int32 = 0
    var u_eRadius : Int32 = 0
    var u_eVelocity : Int32 = 0
    var u_eDecay : Int32 = 0
    var u_eSizeStart : Int32 = 0
    var u_eSizeEnd : Int32 = 0
    var u_eColorStart : Int32 = 0
    var u_eColorEnd : Int32 = 0
    
    init(vertexShader: String, fragmentShader: String) {
        self.compile(vertexShader: vertexShader, fragmentShader: fragmentShader)
    }
    
    func prepareToDraw() {
        glUseProgram(self.programHandle)
        
        
        glUniformMatrix4fv(self.projectionMatrixUniform, 1, GLboolean(GL_FALSE), self.projectionMatrix.array)
        
        glUniformMatrix4fv(self.modelViewMatrixUniform, 1, GLboolean(GL_FALSE), self.modelViewMatrix.array)
        
        // ambient light
        glUniform3f(self.colorUniform, 1, 1, 1)
        glUniform1f(self.lightAmbientIntensityUniform, 0.5);
        
        // diffuse light
        let lightDirection : GLKVector3 = GLKVector3(v: (0, -1, 1))
        glUniform3f(self.lightDirectionUniform, lightDirection.x, lightDirection.y, lightDirection.z)
        glUniform1f(self.lightDiffuseIntensityUniform, 0.7)
        glUniform1f(self.uType, type)
        
        // specular light
        glUniform1f(self.lightSpecularIntensityUniform, 2.0)
        glUniform1f(self.lightShininessUniform, 1.0)
        
        glActiveTexture(GLenum(GL_TEXTURE1))
        glBindTexture(GLenum(GL_TEXTURE_2D), self.texture)
        glUniform1i(self.textureUniform, 1)
    }
}

extension BaseEffect {
    func compileShader(_ shaderName: String, shaderType: GLenum) -> GLuint {
        let path = Bundle.main.path(forResource: shaderName, ofType: nil)
        
        do {
            
            let shaderString = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
            let shaderHandle = glCreateShader(shaderType)
            var shaderStringLength : GLint = GLint(Int32(shaderString.length))
            var shaderCString = shaderString.utf8String
            glShaderSource(
                shaderHandle,
                GLsizei(1),
                &shaderCString,
                &shaderStringLength)
            
            glCompileShader(shaderHandle)
            var compileStatus : GLint = 0
            glGetShaderiv(shaderHandle, GLenum(GL_COMPILE_STATUS), &compileStatus)
            
            if compileStatus == GL_FALSE {
                var infoLength : GLsizei = 0
                let bufferLength : GLsizei = 1024
                glGetShaderiv(shaderHandle, GLenum(GL_INFO_LOG_LENGTH), &infoLength)
                
                let info : [GLchar] = Array(repeating: GLchar(0), count: Int(bufferLength))
                var actualLength : GLsizei = 0
                
                glGetShaderInfoLog(shaderHandle, bufferLength, &actualLength, UnsafeMutablePointer(mutating: info))
                NSLog(String(validatingUTF8: info)!)
                exit(1)
            }
            
            return shaderHandle
            
        } catch {
            exit(1)
        }
    }
    
    func compile(vertexShader: String, fragmentShader: String) {
        let vertexShaderName = self.compileShader(vertexShader, shaderType: GLenum(GL_VERTEX_SHADER))
        let fragmentShaderName = self.compileShader(fragmentShader, shaderType: GLenum(GL_FRAGMENT_SHADER))
        
        self.programHandle = glCreateProgram()
        glAttachShader(self.programHandle, vertexShaderName)
        glAttachShader(self.programHandle, fragmentShaderName)
        
        glBindAttribLocation(self.programHandle, VertexAttributes.position.rawValue, "a_Position") //a_Position
        //a_Color
        glBindAttribLocation(self.programHandle, VertexAttributes.color.rawValue, "a_Color") //a_Color
        glBindAttribLocation(self.programHandle, VertexAttributes.texCoord.rawValue, "a_TexCoord")  // a_TexCoord
        glBindAttribLocation(self.programHandle, VertexAttributes.normal.rawValue, "a_Normal")  // Normal
        
        // Emitter Attributes (Particle System)
        /*glBindAttribLocation(self.programHandle, self.programHandle.rawValue, "a_pID")
        glBindAttribLocation(self.programHandle, self.a_pRadiusOffset.rawValue, "a_pRadiusOffset")
        glBindAttribLocation(self.programHandle, self.a_pVelocityOffset.rawValue, "a_pVelocityOffset")
        glBindAttribLocation(self.programHandle, self.a_pDecayOffset.rawValue, "a_pDecayOffset")
        glBindAttribLocation(self.programHandle, self.a_pSizeOffset.rawValue, "a_pSizeOffset")
        glBindAttribLocation(self.programHandle, self.a_pColorOffset.rawValue, "a_pColorOffset") */
        glLinkProgram(self.programHandle)
        
        //vertexshader fragmentshader
        self.uType = glGetUniformLocation(self.programHandle, "u_type")
        self.modelViewMatrixUniform = glGetUniformLocation(self.programHandle, "u_ModelViewMatrix")
        self.projectionMatrixUniform = glGetUniformLocation(self.programHandle, "u_ProjectionMatrix")
        self.textureUniform = glGetUniformLocation(self.programHandle, "u_Texture")
        self.colorUniform = glGetUniformLocation(self.programHandle, "u_Light.Color")
        self.surfaceColor = glGetUniformLocation(self.programHandle, "u_SurfaceColor")
        // used to be u_SurfaceColor
        
        self.lightAmbientIntensityUniform = glGetUniformLocation(self.programHandle, "u_Light.AmbientIntensity")
        self.lightDiffuseIntensityUniform = glGetUniformLocation(self.programHandle, "u_light.DiffuseIntensity")
        self.lightDirectionUniform = glGetUniformLocation(self.programHandle, "u_Light.Direction")
        self.lightSpecularIntensityUniform = glGetUniformLocation(self.programHandle, "u_Light.SpecularIntensity")
        self.lightShininessUniform = glGetUniformLocation(self.programHandle, "u_Light.Shininess")
        
        // Emitter Uniforms
       /* self.u_Gravity = glGetUniformLocation(self.programHandle, "u_Gravity")
        self.u_Time = glGetUniformLocation(self.programHandle, "u_Time")
        self.u_Texture = glGetUniformLocation(self.programHandle, "u_Texture")
        self.u_ePosition = glGetUniformLocation(self.programHandle, "u_ePosition")
        self.u_eRadius = glGetUniformLocation(self.programHandle, "u_eRadius")
        self.u_eVelocity = glGetUniformLocation(self.programHandle, "u_eVelocity")
        self.u_eDecay = glGetUniformLocation(self.programHandle, "u_eDecay")
        self.u_eSizeStart = glGetUniformLocation(self.programHandle, "u_eSizeStart")
        self.u_eSizeEnd = glGetUniformLocation(self.programHandle, "u_eSizeEnd")
        self.u_eColorStart = glGetUniformLocation(self.programHandle, "u_eColorStart")
        self.u_eColorEnd = glGetUniformLocation(self.programHandle, "u_eColorEnd") */
        
        var linkStatus : GLint = 0
        glGetProgramiv(self.programHandle, GLenum(GL_LINK_STATUS), &linkStatus)
        if linkStatus == GL_FALSE {
            var infoLength : GLsizei = 0
            let bufferLength : GLsizei = 1024
            glGetProgramiv(self.programHandle, GLenum(GL_INFO_LOG_LENGTH), &infoLength)
            
            let info : [GLchar] = Array(repeating: GLchar(0), count: Int(bufferLength))
            var actualLength : GLsizei = 0
            
            glGetProgramInfoLog(self.programHandle, bufferLength, &actualLength, UnsafeMutablePointer(mutating: info))
            NSLog(String(validatingUTF8: info)!)
            exit(1)
        }
    }
}
