//
//  ShaderProcessor.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-04-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit



class ShaderProcessor {
   
init() {}

init(vertexShader: String, fragmentShader: String) {
    //self.BuildProgram(vertexShaderSource: vertexShader, fragmentShaderSource: fragmentShader)
}

func BuildProgram(vertexShaderSource: String, fragmentShaderSource: String) -> GLuint {
    // Build shaders
    let vertexShader : GLuint = self.BuildShader(source: vertexShaderSource, shaderType: GLenum(GL_VERTEX_SHADER))
    let fragmentShader : GLuint = self.BuildShader(source: fragmentShaderSource, shaderType: GLenum(GL_FRAGMENT_SHADER))
    
    // Create Program
    var programHandle : GLuint = glCreateProgram()
    
    // Attach Shaders
    glAttachShader(programHandle, vertexShader)
    glAttachShader(programHandle, fragmentShader)
    
    // Link Program
    glLinkProgram(programHandle)
    
    // Check for errors
    var linkSuccess : GLint? = 0
    glGetProgramiv(programHandle, GLenum(GL_LINK_STATUS), &linkSuccess!)
    if (linkSuccess == GL_FALSE) {
        print("GLSL PROGRAM ERROR: ShaderProcessor.swift")
        var messages : [GLchar?] = [GLchar?](repeating: nil, count: 1024)
        glGetProgramInfoLog(programHandle, GLsizei(MemoryLayout<GLchar>.size * 1024), BUFFER_OFFSET(0), &messages[0]!)
        exit(1)
    }
    
    // Delete shaders
    glDeleteShader(vertexShader)
    glDeleteShader(fragmentShader)
    
    return programHandle
}



func BuildShader(source: String, shaderType: GLenum) -> GLuint {
    let path = Bundle.main.path(forResource: source, ofType: nil)
    
    print("shader name: %s", source)
    
    do {
        
        let shaderString = try NSString(contentsOfFile: path!, encoding: String.Encoding.utf8.rawValue)
        let shaderHandle = glCreateShader(shaderType)
        var shaderStringLength : GLint = GLint(Int32(shaderString.length))
        var shaderCString = shaderString.utf8String
        glShaderSource(
            shaderHandle,
            GLsizei(1),
            &shaderCString,
            &shaderStringLength)
        
        glCompileShader(shaderHandle)
        var compileStatus : GLint = 0
        glGetShaderiv(shaderHandle, GLenum(GL_COMPILE_STATUS), &compileStatus)
        
        if compileStatus == GL_FALSE {
            var infoLength : GLsizei = 0
            let bufferLength : GLsizei = 1024
            glGetShaderiv(shaderHandle, GLenum(GL_INFO_LOG_LENGTH), &infoLength)
            
            let info : [GLchar] = Array(repeating: GLchar(0), count: Int(bufferLength))
            var actualLength : GLsizei = 0
            
            glGetShaderInfoLog(shaderHandle, bufferLength, &actualLength, UnsafeMutablePointer(mutating: info))
            NSLog(String(validatingUTF8: info)!)
            exit(1)
        }
        return shaderHandle
        
    } catch {
        print("COULD NOT COMPILE SHADER:  ShaderProcessor.swift")
        exit(1)
    }
}
    
    
func BUFFER_OFFSET(_ n: Int) -> UnsafeMutablePointer<GLsizei>? {
    return UnsafeMutablePointer<GLsizei>(bitPattern: n)
}

}
