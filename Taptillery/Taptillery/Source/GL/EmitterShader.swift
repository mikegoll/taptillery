//
//  EmitterShader.swift
//  Taptillery
//
//  Created by Zac Koop on 2018-04-15.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import GLKit

class EmitterShader {
    
    var program : GLuint = 0
    
    //Particle Emitter Attributes
    var a_pID : Int32 = 0
    var a_pRadiusOffset : Int32 = 0
    var a_pVelocityOffset : Int32 = 0
    var a_pDecayOffset : Int32 = 0
    var a_pSizeOffset : Int32 = 0
    var a_pColorOffset : Int32 = 0
    
    // Particle Emitter Uniforms
    var u_ProjectionMatrix : Int32 = 0
    var u_Gravity : Int32 = 0
    var u_Time : Int32 = 0
    var u_Texture : Int32 = 0
    var u_ePosition : Int32 = 0
    var u_eRadius : Int32 = 0
    var u_eVelocity : Int32 = 0
    var u_eDecay : Int32 = 0
    var u_eSizeStart : Int32 = 0
    var u_eSizeEnd : Int32 = 0
    var u_eColorStart : Int32 = 0
    var u_eColorEnd : Int32 = 0
    
    
init(){}

func loadShader() {
    // Program
    let shaderProcessor : ShaderProcessor = ShaderProcessor();
    self.program = shaderProcessor.BuildProgram(vertexShaderSource:"VertEmitter.glsl", fragmentShaderSource:"FragEmitter.glsl")
    
    // Attributes
    self.a_pID = glGetAttribLocation(self.program, "a_pID")
    self.a_pRadiusOffset = glGetAttribLocation(self.program, "a_pRadiusOffset")
    self.a_pVelocityOffset = glGetAttribLocation(self.program, "a_pVelocityOffset")
    self.a_pDecayOffset = glGetAttribLocation(self.program, "a_pDecayOffset")
    self.a_pSizeOffset = glGetAttribLocation(self.program, "a_pSizeOffset")
    self.a_pColorOffset = glGetAttribLocation(self.program, "a_pColorOffset")
    
    // Uniforms
    self.u_ProjectionMatrix = glGetUniformLocation(self.program, "u_ProjectionMatrix")
    self.u_Gravity = glGetUniformLocation(self.program, "u_Gravity")
    self.u_Time = glGetUniformLocation(self.program, "u_Time")
    self.u_Texture = glGetUniformLocation(self.program, "u_Texture")
    self.u_ePosition = glGetUniformLocation(self.program, "u_ePosition")
    self.u_eRadius = glGetUniformLocation(self.program, "u_eRadius")
    self.u_eVelocity = glGetUniformLocation(self.program, "u_eVelocity")
    self.u_eDecay = glGetUniformLocation(self.program, "u_eDecay")
    self.u_eSizeStart = glGetUniformLocation(self.program, "u_eSizeStart")
    self.u_eSizeEnd = glGetUniformLocation(self.program, "u_eSizeEnd")
    self.u_eColorStart = glGetUniformLocation(self.program, "u_eColorStart")
    self.u_eColorEnd = glGetUniformLocation(self.program, "u_eColorEnd")
}

}
