//
//  ViewController.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-01-17.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import UIKit
import GLKit
//Music Imports
import AVFoundation
import MediaPlayer

class GLKUpdater : NSObject, GLKViewControllerDelegate {
    
    weak var glkViewController : ViewController!
    
    init(glkViewController : ViewController) {
        self.glkViewController = glkViewController
    }
    
    //MARK: UPDATE OPENGL FUNCTION
    func glkViewControllerUpdate(_ controller: GLKViewController) {
//        for i in glkViewController.cubes {
//            i.updateWithDelta(self.glkViewController.timeSinceLastUpdate)
//        }
        //MARK: This is what makes the cube spin!
        if (glkViewController.cube != nil) {
            glkViewController.cube.updateWithDelta(self.glkViewController.timeSinceLastUpdate)
        }
        if (glkViewController.model != nil) {
            glkViewController.model.updateWithDelta(self.glkViewController.timeSinceLastUpdate)
        }
        
        
        if (glkViewController.emitters.count != 0) {
            var deadEmitters : [EmitterObject?] = []
            
            for emitter in glkViewController.emitters {
                var alive : Bool = (emitter?.updateWithDelta(Float(self.glkViewController.timeSinceLastUpdate)))!
                
                if (!alive) {
                    deadEmitters.append(emitter)
                }
            }
            
            for i in 0 ..< deadEmitters.count {
                self.glkViewController.emitters.remove(at: i)
            }
        }
    }
}



class ViewController: GLKViewController {
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var unitView: UIView!
    
    @IBOutlet weak var playerTag: UILabel!
    
    @IBOutlet weak var unitTypeTag: UILabel!
    @IBOutlet weak var healthTag: UILabel!
    @IBOutlet weak var damageTag: UILabel!
    
    private var db : DBHelper!
    
    //MARK: Audio
    var audioPlayer = AVAudioPlayer()
    
    //MARK: Managers
    var gameManager : GameManager!
    var textureManager: TextureManager!
    
    //MARK: Shaders
    var shader : BaseEffect!
    var shader2 : BaseEffect!
    var particleShader : EmitterShader!
    var glkView: GLKView!
    var glkUpdater: GLKUpdater!
    let viewMatrix : GLKMatrix4 = GLKMatrix4MakeTranslation(0, 0, -5)
    
    //MARK: Models
    var gridSystem : GridController!
    var cube : Cube!

    var moving : Bool! = false
    var rotated : Bool = false
    var model : Model!
    
    // PARTICLES
    var emitters : [EmitterObject?] = []
    var projectionMatrix : GLKMatrix4?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.db = DBHelper()
        self.setupGLcontext()
        self.setupGLupdater()
        self.setupTextures() //Setup Textures
        self.setupScene()
        self.setupMusic()

        let tags : [UILabel] = [playerTag, unitTypeTag, healthTag, damageTag]
        let views : [UIView] = [generalView, unitView]
        let ui : UIManager = UIManager(tags: tags, views: views)
        
        // Create a new emitter object
        var emitter : EmitterObject = EmitterObject(texture: "texture_64.png", position: GLKVector2Make(0,0))
        self.emitters.append(emitter)
        
        gameManager = GameManager(grid : self.gridSystem, be : self.shader, uiManager: ui, textureManager: self.textureManager)
        gameManager.startGame()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        //Transfomr4: Viewport: Normalized -> Window
        //glViewport(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        //GLKit
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT))
        
        glEnable(GLenum(GL_DEPTH_TEST))
        glEnable(GLenum(GL_CULL_FACE))
        glEnable(GLenum(GL_BLEND))
        glBlendFunc(GLenum(GL_SRC_ALPHA), GLenum(GL_ONE_MINUS_SRC_ALPHA))
        
//        self.square.renderWithParentMoelViewMatrix(viewMatrix)
        
        if (self.gridSystem != nil) {
            let grid = self.gridSystem.getGrid()
            for i in 0 ..< grid.count {
                for j in 0 ..< grid[i].count {
                    grid[i][j].renderWithParentMoelViewMatrix(self.viewMatrix)
                    if(grid[i][j].getUnitID() > -1) {
                        let unit = grid[i][j].getUnit();
                        let id : Int! = grid[i][j].getUnitID();
//                        if (unit != nil) {
                            if(gameManager.unitIsAlive(id : id)){
                                unit.renderWithParentMoelViewMatrix(GLKMatrix4MakeTranslation(0, 0, -5))
                            } else {
                                print("dead")
                                _ = grid[i][j].setUnitID(newUnitID: -1)
                                
                            }
//                        }
                    }
                }
            }
        }
        
        //self.model.renderWithParentMoelViewMatrix(viewMatrix)
        //self.model.setColor(red: 0.0, green: 0.0, blue: 1.0);
        
        if(moving) {
            if(gameManager.curCell != nil) {
                updateUnitPos()
            }
        }
        
        if (self.emitters.count != 0) {
            for i in 0..<self.emitters.count {
                emitters[i]?.renderWithProjection(projectionMatrix: projectionMatrix!)
            }
        }
//        for i in cubes {
//            i.renderWithParentMoelViewMatrix(viewMatrix)
//        }
        
//        self.cube.position.x = 5
//        self.cube.renderWithParentMoelViewMatrix(viewMatrix)
    }
    
    
    
    
    //    @IBAction func endTurn(_ sender: UIButton) {
    //MARK: No player is initalized
    
    //        if(Player.getPlayerID() == 1){
    //            Player.setPlayerID(2);
    //        }else{
    //            Player.setPlayerID(1);
    //        }
    //
    //        playerText.text = "PLAYER \(Player.getPlayerID())";
    //
    //    }
    
    @IBAction func EndTurnButton(_ sender: Any) {
        if(gameManager.curCell != nil) {
            unselectCell(cell: gameManager.curCell)
            gameManager.deselectCurCell()
        }
        gameManager.resetArmy()
        _ = gameManager.endTurn()
    }
    
    @IBAction func ExitButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "main") as! MainViewController
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func TapGesture(_ sender: UITapGestureRecognizer) {
        //let test =
        //let test = [sender, translationInView:self.view];
        //print("Point: \(test.x) : \(test.y)");
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Get coordinates from touch in screen window
        //Get projection matrix and view matrix (if no model required)
        //multiply projection * view
        //inverse results of multiplication
        //construct vector with
        //  x - touch position x within range of window (transform value between -1 and 1)
        //  y - touch position y within range of window (transform value between -1 and 1)
        //  z - depth value, can manually go from -1 to 1
        //  w = 1.0
        //multiply vector by inverse matrix created
        //divide result vector by w component after matrix multiplication
        
        if let touch = touches.first {
            var unitId : Int!
            let position = touch.location(in: view)
            
            let w = 1.0;
            
            let projMatrix = GLKMatrix4MakePerspective(
                GLKMathDegreesToRadians(90.0),
                GLfloat(self.view.bounds.size.width / self.view.bounds.size.height),
                1,
                150);
            
            let resultMatrix = GLKMatrix4Multiply(projMatrix, viewMatrix);
            
            var result : Bool = false;
            let inverseResultMatrix = GLKMatrix4Invert(resultMatrix, &result);
            
            if (result) {
                //was inverted
                let resultVector = GLKVector4Make(Float(position.x), Float(position.y), Float(1.0), Float(w));
                
                var resultMatrixAfterInverse = GLKMatrix4MultiplyVector4(inverseResultMatrix, resultVector);
                resultMatrixAfterInverse = GLKVector4DivideScalar(resultMatrixAfterInverse, Float(w));
                
                print("\n----- Touch Position -----\n x: \(position.x), y: \(position.y)\n")
                print("----- Resultant Vector -----\n x: \(resultMatrixAfterInverse.x), y: \(resultMatrixAfterInverse.y), z: \(resultMatrixAfterInverse.z), w: \(resultMatrixAfterInverse.w)\n")
                
                // Create a new emitter object
                var emitter : EmitterObject = EmitterObject(texture: "texture_64.png", position: GLKVector2Make(Float(position.x), Float(position.y)))
                self.emitters.append(emitter)
                print("Emitter created")
                
                //scale x and y between 0 and 1
                let tempX = resultMatrixAfterInverse.x / Float(UIScreen.main.bounds.size.width * 2);
                let tempY = resultMatrixAfterInverse.y / Float(UIScreen.main.bounds.size.height);
                
                print("\nwidth:\(UIScreen.main.bounds.size.width * 2), height: \(UIScreen.main.bounds.size.height)")
                
                print("\n----- TEMPS -----\n x: \(tempX), y: \(tempY)\n")
                
                let cell : Cell = self.gridSystem.getCellByScrn(x: tempX, y: tempY)!
                //checking if unit is in cell
                unitId = cell.getUnitID();
                if(unitId > -1 && !moving) {
                    if(gameManager.unitInPlayersArmy(id: unitId)) {
                        selectUnit(cell: cell);
                    } else if(gameManager.hasAttackCells()) {
                        attemptAttack(cell : cell)
                        
                        if(gameManager.gameOver){
                            //GAME OVER SWITCH TO HOME SCREEN VIEW************
                            
                            //1. Create the alert controller.
                            let alert = UIAlertController(title: "Highscores", message: "Enter your name", preferredStyle: .alert)
                            
                            //2. Add the text field. You can configure it however you need.
                            alert.addTextField { (textField) in
                                textField.text = ""
                            }
                            
                            // 3. Grab the value from the text field, and print it when the user clicks OK.
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                                print("Text field: \(String(describing: textField?.text))")
                                
                                //DBHelper here
                                //gameManager.finalScore
                                let value : String = "" + (textField?.text)!
                                _ = self.db.addHighscore(userid: self.db.getAllHighscores().count, playername: "\(value)", points: self.self.gameManager.finalScore)
                                
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "main") as! MainViewController
                                self.present(vc, animated: true, completion: nil)
                            }))
                            
                            // 4. Present the alert.
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                } else if (!moving){
                    if(gameManager.curCell != nil) {
                        if(gameManager.cellInMoveRange(cell: cell) && !gameManager.getUnitByID(id: gameManager.curCell.getUnitID()).moved) {
                            gameManager.destination = cell.getPosition()
                            moving = true
                            gameManager.destCell = cell
                            print("moveable cell")
                        }
                    }
                }
            } else {
                //cannot be inverted
                print("--------------- Cannot be inverted ---------------\n");
            }
        }
    }
}

//MARK: Extensionsss
extension ViewController {
    
    func setupMusic() {
        print("Setting up Music")
        let path = Bundle.main.url(forResource: "Wind-Waker-Ocean-Theme", withExtension: "mp3")
        do{
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer = try AVAudioPlayer(contentsOf: path!)
//            audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            audioPlayer.play()
        }catch{
            print("Audio failed to play")
        }
        //To pause
//        audioPlayer.pause()
    }
    
    func setupTextures() {
        self.textureManager = TextureManager()
    }
    
    func setupGLcontext() {
        glkView = self.view as! GLKView
        glkView.context = EAGLContext(api: .openGLES2)!
        glkView.drawableDepthFormat = .format16         // for depth testing
        EAGLContext.setCurrent(glkView.context)
    }
    
    func setupGLupdater() {
        self.glkUpdater = GLKUpdater(glkViewController: self)
        self.delegate = self.glkUpdater
    }
    
    func setupScene() {
        //MARK: Creates the glsl shader files
        self.shader = BaseEffect(vertexShader: "SimpleVertexShader.glsl", fragmentShader: "SimpleFragmentShader.glsl")
        //MARK: Camera Persepective
        self.shader.projectionMatrix = GLKMatrix4MakePerspective(
            GLKMathDegreesToRadians(90.0),
            GLfloat(self.view.bounds.size.width / self.view.bounds.size.height),
            1,
            150)
        
        projectionMatrix = self.shader.projectionMatrix
        
        //MARK: Camera Persepective
        self.gridSystem = GridController(cellShader: self.shader, textureManager: self.textureManager, resX: Float(self.view.bounds.size.width), resY: Float(self.view.bounds.size.height), rowSize: Settings.row, columnSize: Settings.column)
        
        if (self.gridSystem != nil) {
            let grid = self.gridSystem.getGrid()
            for i in 0 ..< grid.count {
                for j in 0 ..< grid[i].count {
                    //grid[i][j].renderWithParentMoelViewMatrix(self.viewMatrix)
                    _ = grid[i][j];
                }
            }
        }
        
        // load obj file named "key.obj"
        /*let fixtureHelper = FixtureHelper()
        let source = try? fixtureHelper.loadObjFixture(name: "tank")
        
        if let source = source {
            let loader = ObjLoader(source: source, basePath: fixtureHelper.resourcePath)
            do {
                let shapes = try loader.read()
                
                self.model = ObjModel(name: "tank", shape: shapes.first!, shader: self.shader)
                self.model.scale = 0.4
            } catch {
                print("Parsing failed with unknown error")
            }
        } else {
            print("obj not found")
        }*/
    }
    
    //selects tapped unit/cell and updates movementcell array in game manager
    func selectUnit(cell : Cell) {
        let range : Int!
        var mPoints : Int!;
        let unitId : Int! = cell.getUnitID()
        let row : Int! = cell.getRow()
        let col : Int! = cell.getCol()
        var cells : [Cell]!
        range = gameManager.getAttackRange(id: unitId)
        mPoints = gameManager.getMovePoints(id: unitId)
        if(gameManager.curCell != nil) {
            unselectCell(cell : gameManager.curCell)
            gameManager.curCell.setColor(red: 20/256, green: 117/256, blue: 5/256)
            gameManager.curCell.setTexture(file: textureManager.textures[0])
            //MARK: Setting Current Cell to Green
        }
        
        gameManager.selectCell(cell: cell)
        gameManager.curCell.setColor(red: 255/256, green: 250/256, blue: 0.0)
        gameManager.curCell.setTexture(file: textureManager.textures[0])
        //MARK: Setting Yellow
        
        if(gameManager.getUnitByID(id: unitId).hasAttack){
            cells = gameManager.setAttackReticleColours(texture: textureManager.textures[2], red: 1.0, green: 0.0, blue: 0.0, radius: range, row: row, col: col, gridSystem: self.gridSystem)
            gameManager.setAttackCells(cells: cells)
        }
        
        if(gameManager.getUnitByID(id: unitId).moved == false) {
            cells = gameManager.setMovementReticleColours(texture: textureManager.textures[3], red: 0.0, green: 203/256, blue: 255/256, radius: mPoints, row: row, col: col, gridSystem: self.gridSystem)
            gameManager.setMovementCells(cells: cells)
        }
    }
    
    //unselects last selected cell
    func unselectCell(cell : Cell) {
        gameManager.resetReticleColors()
        //movement
        gameManager.curCell.setTexture(file: textureManager.textures[0])
        gameManager.curCell.setColor(red: 20/256, green: 117/256, blue: 5/256)
        //MARK: Reseting Cell back to green
    }
    
    func updateUnitPos() {
        let curX : Float! = gameManager.curCell.getUnit().position.x
        let curY : Float! = gameManager.curCell.getUnit().position.y
        let destX : Float! = gameManager.destination.x
        let destY : Float! = gameManager.destination.y
        
        if (!rotated) {
            enableActionView(state: true)
            rotated = !rotated
        }
        
        if(abs(curX - destX) > 0.1) {
            if(destX > curX) {
                gameManager.curCell.getUnit().position.x += 0.1
            } else {
                gameManager.curCell.getUnit().position.x -= 0.1
            }
        } else if (abs(curY - destY) > 0.1) {
            if(destY > curY) {
                gameManager.curCell.getUnit().position.y += 0.1
            } else {
                gameManager.curCell.getUnit().position.y -= 0.1
            }
        } else {
            moving = false
        }
        
        if(moving == false) {
            gameManager.getUnitByID(id: gameManager.curCell.getUnitID()).hasMoved()
            unselectCell(cell: gameManager.curCell)
            gameManager.destCell.setUnit(model: gameManager.curCell.getUnit())
            _ = gameManager.destCell.setUnitID(newUnitID: gameManager.curCell.getUnitID())
            _ = gameManager.curCell.setUnitID(newUnitID : -1)
            gameManager.deselectCurCell()//unselects objects lest cell
            gameManager.selectCell(cell: gameManager.destCell)//sets selected cell to objects new destination
            if (rotated) {
                enableActionView(state: false)
                rotated = !rotated
            }
        }
    }

    func enableActionView(state : Bool) {
        let rotation : Float = 45.0
        let translation : Float = 15.0
        let cameraTrans : Float = 5.0
        
        if (state) {
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, 0, cameraTrans)
            self.shader.projectionMatrix = GLKMatrix4RotateX(self.shader.projectionMatrix, GLKMathDegreesToRadians(-rotation))
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, translation, 0)
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, 0, -cameraTrans)
        } else {
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, 0, cameraTrans)
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, -translation, 0)
            self.shader.projectionMatrix = GLKMatrix4RotateX(self.shader.projectionMatrix, GLKMathDegreesToRadians(rotation))
            self.shader.projectionMatrix = GLKMatrix4Translate(self.shader.projectionMatrix, 0, 0, -cameraTrans)
        }
    }
    
    func attemptAttack(cell : Cell) {
        let curCell : Cell! = gameManager.curCell
        if(gameManager.cellInAttackRange(cell: cell)){
            gameManager.attackUnit(cell : cell)
            
            let range : Int!
//            let mPoints : Int!;
//            let unitId : Int! = cell.getUnitID()
            let row : Int! = curCell.getRow()
            let col : Int! = curCell.getCol()
            
            range = gameManager.getAttackRange(id: curCell.getUnitID())
            _ = gameManager.getMovePoints(id: curCell.getUnitID())
            _ = gameManager.setAttackReticleColours(texture: textureManager.textures[0], red: 20/256, green: 117/256, blue: 5/256, radius: range, row: row, col: col, gridSystem: self.gridSystem)
        }
    }
}
