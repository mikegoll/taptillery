//
//  HighscoreController.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-04-12.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import UIKit

class HighscoreController : UIViewController {
    private var db : DBHelper!
    @IBOutlet weak var highscorelist: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = DBHelper()
        
        _ = db.addHighscore(userid: 0, playername: "player", points: 10)
        _ = db.addHighscore(userid: 0, playername: "player2", points: 10)
        _ = db.addHighscore(userid: 0, playername: "player3", points: 10)
        let highscore : [Highscore] = db.getAllHighscores()
        
        var scoretexts = ""
        for score in highscore {
            scoretexts += "\(score.id) \(score.playername) \(score.points) \n"
        }
        highscorelist.text = scoretexts
        
        //MARK: Remove all scores
        self.removeScores()
    }
    
    func removeScores() {
        db.deleteAllHighscores()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
