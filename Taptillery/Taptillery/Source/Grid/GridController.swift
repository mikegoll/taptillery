//
//  GrindController.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-01-19.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import UIKit

class GridController {
    private var GridSystem: Array<Array<Cell>>
    private var cellShader : BaseEffect
    private var cellSize : CGPoint
    private let rowSize : Float
    private let columnSize : Float
    private var textureManager: TextureManager!
    
    //MARK: GridSystem Initializer
    init?(cellShader: BaseEffect, textureManager: TextureManager, resX: Float, resY: Float, rowSize : Float, columnSize: Float) {
        //Returns nil if expression is not met
        guard !resX.isNaN || !resY.isNaN || rowSize != 0 || columnSize != 0 else {
            return nil
        }
        
        self.cellShader = cellShader;
        self.textureManager = textureManager;
        self.GridSystem = Array(Array())
        
        let screenSize = UIScreen.main.bounds
        print("\n ------ Setting up Grid System ------")
        print("Screen: \(screenSize.width) - \(screenSize.height)")
        print("row: \(rowSize) - column: \(columnSize)")
        
        self.rowSize = rowSize
        self.columnSize = columnSize
        
        // 375 / 7 = height / row = cell's height
        // 667 / 13 = width / column = cell's width
        let cellHeight = Float(screenSize.height) / rowSize
        let cellWidth = Float(screenSize.width) / columnSize
        print("Cell: \(cellWidth) - \(cellHeight)")
        cellSize = CGPoint(x: CGFloat(cellWidth), y: CGFloat(cellHeight))

        var currenti : Float = -floor(rowSize/2)
        var currentj : Float = -floor(columnSize/2)
        
        var x = 0
        var y = 0
        
        while (currenti < floor(rowSize/2) + 1) {
            currentj = -floor(columnSize/2)
            y = 0
            var cells : Array<Cell> = Array()
            while (currentj < floor(columnSize/2) + 1) {
                let cell : Cell = Cell(shader: self.cellShader, x: currentj * Settings.spacing, y: currenti * Settings.spacing, z: -5.0, scale: 1.30, row: x, column: y)
                cell.setTexture(file: textureManager.textures[0])
                cells.insert(cell, at: y)
                currentj += 1.0
                y += 1
            }
            self.GridSystem.insert(cells, at: x)
            currenti += 1.0
            x += 1
        }
    }
    
    //MARK: Returns GridSystem Array
    func getGrid() -> Array<Array<Cell>> {
        return self.GridSystem
    }
    
    //MARK: Returns cell based on array values
    func getCell(row: Int, column: Int) -> Cell? {
        guard row >= 0 || column >= 0 else {
            print("getCell: Failed to pass below 0 check")
            return nil
        }
        return self.GridSystem[row][column]
    }
    
    //MARK: Returns cell based on screen values
    func getCellByScrn(x: Float, y: Float) -> Cell? {
        //print("\n---- Getting Cell by Screen ----")
        //print("\nINPUTED: \(x) - \(y)")
        //print("Cell Size: \(cellSize.x) \(cellSize.y)")
        
        print("x: \(x * columnSize), y: \(y * rowSize)\n")
        
        let tempX : Float = (x * columnSize)
        let tempY : Float = (y * rowSize)
        
        var estXPos : Int
        var estYPos : Int
        
        if (tempX > 8) {
            estXPos = Int(ceil(tempX)) // = width
        } else if (tempX < 1) {
            estXPos = Int(floor(tempX)) // = width
        } else {
            estXPos = Int(round(tempX)) // = width
        }
        
        estYPos = Int(abs(tempY - rowSize)) // = height
        
        print("\nestx: \(estXPos), esty: \(estYPos)\n")
    
        return self.getCell(row: estYPos, column: estXPos)
    }
    
    func getGridSize() -> [Int] {
        return [self.GridSystem.count, self.GridSystem[0].count]
    }
    
    func moveUnitFromCellToCell (currentCell : Cell, newCell: Cell) -> Bool {
        if (currentCell.row == newCell.row && currentCell.column == newCell.column) {
            print("Failed to move unit")
            return false
        }
        //let cell : Cell = self.getCell(row: currentCell.row, column: currentCell.column)
        let unit = self.getCell(row: currentCell.row, column: currentCell.column)?.getUnitID()
        _ = self.getCell(row: currentCell.row, column: currentCell.column)?.setUnitID(newUnitID: -1)
        _ = self.getCell(row: currentCell.row, column: currentCell.column)?.setUnitID(newUnitID: unit!)
        return true
    }
    
    //Will return nil if a cell is a border
    func getRangeOfCells(initRow: Int, initColumn: Int, finRow: Int, finColumn: Int) -> [Cell]? {
        var path = [Cell]()
        if (initRow > finRow) {
            if (initColumn > finColumn) {
                //Down Left
                for i in finRow ..< initRow {
                    for j in finColumn ..< initColumn {
                        let c = self.GridSystem[i][j]
                        //check if c is border
                        if (c.IsBorder()) {
                            return nil
                        }
                        path.append(c)
                    }
                }
            } else {
                //Down or Equal Columns
                //Down Right
                for i in finRow ..< initRow {
                    for j in initColumn ..<  finColumn{
                        let c = self.GridSystem[i][j]
                        //check if c is border
                        if (c.IsBorder()) {
                            return nil
                        }
                        path.append(c)
                    }
                }
            }
        } else {
            //Top or Equal Rows
            if (initColumn > finColumn) {
                //Top Left
                for i in initRow ..<  finRow{
                    for j in finColumn ..<  initColumn{
                        let c = self.GridSystem[i][j]
                        //check if c is border
                        if (c.IsBorder()) {
                            return nil
                        }
                        path.append(c)
                    }
                }
            } else {
                //Top or Equal Columns
                //Top Right
                for i in finRow ..<  initRow{
                    for j in finColumn ..<  initColumn{
                        let c = self.GridSystem[i][j]
                        //check if c is border
                        if (c.IsBorder()) {
                            return nil
                        }
                        path.append(c)
                    }
                }
            }
        }
        return path
    }
    
    func spawnArmies(army: [Int], player : Int) {
        if(player == 1) {
            
        } else if(player == 2) {
            
        } else {
            print("******ERROR IN GRID CONTROLLER: ROGUE PLAYER")
        }
    }
}
