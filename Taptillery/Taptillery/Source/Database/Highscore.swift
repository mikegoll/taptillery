//
//  Highscore.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-03-02.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation

class Highscore {
    var id : Int64 = 0
    var userid: Int = 0
    var playername: String = "default"
    var points : Int = 0
    
    init(id: Int64, userid: Int, playername: String, points: Int) {
        self.id = id
        self.userid = userid
        self.playername = playername
        self.points = points
    }
}
