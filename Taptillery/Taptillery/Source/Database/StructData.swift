//
//  StructData.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-01-17.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import SQLite

struct StructData {

    //MARK: Driver Table Definition
    struct Highscores {
        static let highscores = Table("highscores")
        //primary key
        static let id = Expression<Int64>("id")
        //columns
        static let userid = Expression<Int>("userid")
        static let playername = Expression<String>("firstname")
        static let points = Expression<Int>("points")
    }
}

