//
//  DBHelper.swift
//  Taptillery
//
//  Created by Dalton Danis on 2018-01-17.
//  Copyright © 2018 DigitalMob. All rights reserved.
//

import Foundation
import SQLite

class DBHelper {

    static let instance = DBHelper()
    private let db: Connection?

    //Link to SQLite documentation:
    //https://github.com/stephencelis/SQLite.swift
    init() {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
            ).first!

        do {
            db = try Connection("\(path)/db.sqlite3")
        } catch {
            db = nil
            print ("Unable to open database")
        }
        createTables()
    }

    //MARK: Creating Tables
    func createTables() {

        //MARK: Highscore Table Creation
        do {
            try db!.run(StructData.Highscores.highscores.create { t in
                t.column(StructData.Highscores.id, primaryKey: true)
                t.column(StructData.Highscores.userid)
                t.column(StructData.Highscores.playername)
                t.column(StructData.Highscores.points)
            })
        } catch {
            print("Unable to create user table")
        }
    }

    //-----------------------------
    //MARK: User Table Functionality
    //-----------------------------

    //MARK: add a highscore
    func addHighscore(userid: Int, playername: String, points: Int) -> Int64? {
        do {
            let insert = StructData.Highscores.highscores.insert(
                StructData.Highscores.userid <- userid,
                StructData.Highscores.playername <- playername,
                StructData.Highscores.points <- points
            )
            print(insert.asSQL())
            let id = try db!.run(insert)

            return id
        } catch {
            print("Highscores Insert Failed")
            return -1
        }

    }

    //MARK: delete a highscore
    func deleteHighscore(id: Int64) -> Bool {
        do {
            let highscore = StructData.Highscores.highscores.filter(StructData.Highscores.id == id)
            try db!.run(highscore.delete())
            return true
        } catch {
            print("Delete failed")
        }
        return false
    }

    //MARK: update a highscore by id
    func updateHighscore(id: Int64, newHighscore: Highscore) -> Bool {
        let highscore = StructData.Highscores.highscores.filter(StructData.Highscores.id == id)
        do {
            let update = highscore.update([
                StructData.Highscores.userid <- newHighscore.userid,
                StructData.Highscores.playername <- newHighscore.playername,
                StructData.Highscores.points <- newHighscore.points,
                ])
            if try db!.run(update) > 0 {
                return true
            }
        } catch {
            print("Update failed: \(error)")
        }

        return false
    }

    //MARK: get all highscores
    func getAllHighscores() -> [Highscore] {
        var highscores = [Highscore]()

        do {
            for highscore in try db!.prepare(StructData.Highscores.highscores) {
                highscores.append(Highscore(
                    id: highscore[StructData.Highscores.id],
                    userid: highscore[StructData.Highscores.userid],
                    playername: highscore[StructData.Highscores.playername],
                    points: highscore[StructData.Highscores.points]
                ))
            }
        } catch {
            print("Select failed")
        }

        return highscores
    }

    //MARK: get all highscore
    func getHighscore(id: Int) -> Highscore? {
        do {
            var count = 0
            for highscore in try db!.prepare(StructData.Highscores.highscores) {
                if (count == id) {
                    return Highscore(
                        id: highscore[StructData.Highscores.id],
                        userid: highscore[StructData.Highscores.userid],
                        playername: highscore[StructData.Highscores.playername],
                        points: highscore[StructData.Highscores.points]
                    )
                }
                count += 1
            }
        } catch {
            print("Select failed")
        }
        
        print("Select failed: Highscore not found")
        return nil
    }

    //MARK: prints all highscores id's
    func printHighscores() {
        do {
            for highscore in try db!.prepare(StructData.Highscores.highscores) {
                print(highscore[StructData.Highscores.id])
                print(highscore[StructData.Highscores.userid])
            }
        } catch {
            print("Select failed")
        }
    }

    //MARK: get number of highscores
    func getNumberOfHighscores() -> Int {
        var count: Int = 0
        do {
            for _ in try db!.prepare(StructData.Highscores.highscores) {
                count += 1
            }
        } catch {
            print("Select failed")
        }
        return count
    }

    //MARK: delete all highscores
    func deleteAllHighscores() {
        do {
            for highscore in try db!.prepare(StructData.Highscores.highscores) {
                print(highscore[StructData.Highscores.id])
                if (self.deleteHighscore(id: highscore[StructData.Highscores.id])) {
                    print("Deleted Highscore")
                } else {
                    print("Failed to Delete Highscore")
                }
            }
        } catch {
            print("Select failed")
        }
    }
}


